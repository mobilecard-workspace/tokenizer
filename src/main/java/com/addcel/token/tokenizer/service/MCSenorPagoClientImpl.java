package com.addcel.token.tokenizer.service;

import java.text.DecimalFormat;
import java.util.Base64;
import java.util.Collections;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.token.tokenizer.bean.Authorization;
import com.addcel.token.tokenizer.bean.BasePayment;
import com.addcel.token.tokenizer.bean.SrPagoCheckout;
import com.addcel.token.tokenizer.bean.srpago.SrPagoRequest;
import com.addcel.token.tokenizer.bean.srpago.SrPagoResponse;
import com.addcel.token.tokenizer.model.domain.Tarjetas;
import com.addcel.token.tokenizer.model.repository.AuthorizationRepository;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
@PropertySource(ignoreResourceNotFound = true, value = "classpath: application.properties")
public class MCSenorPagoClientImpl implements MCSenorPagoClient {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MCSenorPagoClientImpl.class); 
	
	@Autowired
    private RestTemplate restTemplate;
	
	private Gson GSON = new Gson();

    @Autowired
    private AuthorizationRepository authorizationRepository;
    
    //propiedades del cliente
    @Value("${sr.pago.user}")
    private String user;
    @Value("${sr.pago.pass}")
    private String pass;
    @Value("${sr.pago.url}")
    private String url;

	@Override
	public SrPagoCheckout checkOut(Integer idApp, Integer idPais, String idioma, BasePayment pago, Tarjetas tarjeta) {
		SrPagoCheckout resp = new SrPagoCheckout();
        SrPagoRequest request = new SrPagoRequest();
        SrPagoResponse check = new SrPagoResponse();
        Authorization idBitacora = null;
        double total = 0;
        double cargo = 0;
        double comision = 0;
        double propina = 0;
        double amount = 0;
        DecimalFormat df = new DecimalFormat("#.00");
        
        try {
        	LOGGER.info("SOLICITANDO CHECKOUT PARA 3D...");
            LOGGER.info("SOLICITANDO CHECKOUT - {}", GSON.toJson(pago));
            LOGGER.info("CARGO - COMISION  - PROPINA - {} - {} - {}", df.format(pago.getCargo()).replace(",", "."),
                    df.format(pago.getComision()).replace(",", "."),
                    df.format(pago.getPropina()).replace(",", "."));
            cargo = Double.valueOf(df.format(pago.getCargo()).replace(",", "."));
            LOGGER.info("CARGO - {}", cargo);
            comision = Double.valueOf(df.format(pago.getComision()).replace(",", "."));
            LOGGER.info("COMISION - {}", comision);
            propina = Double.valueOf(df.format(pago.getPropina()).replace(",", "."));
            LOGGER.info("PROPINA - {}", propina);
            if(pago.getCargo() == 0){
                amount = Double.valueOf(df.format(pago.getAmount()).replace(",", "."));
                pago.setCargo(amount);
                cargo = amount;
            }
            total = cargo + comision + propina;
            LOGGER.info("CALCULANDO MONTO A PAGAR  {}", total);
            
            idBitacora = getIdBitacora(idApp, idPais, idioma, pago);
            LOGGER.info("GET ID BITACORA RESPONSE: {}", GSON.toJson(idBitacora));
            if (idBitacora.getCode() == 0) {
            	//Se arma el request que requiere srpago
				request.setAmount(total);
				request.setReference(idBitacora.getIdTransaccion()+"");
				if(pago.getIdCard() != 0) {
					request.setCvv(AddcelCrypto.decryptTarjeta(tarjeta.getCt()));
					request.setHolder(tarjeta.getNombre().replaceAll(Pattern.quote(" "), ""));
					String vigencia = AddcelCrypto.decryptTarjeta(tarjeta.getExpDate());
					String[] exp = vigencia.split(Pattern.quote("/"));
					request.setMonth(exp[0]);
					request.setYear(exp[1]);
					request.setNumber(AddcelCrypto.decryptTarjeta(tarjeta.getPan()));
				}else {
					resp.setCode(-100);
					resp.setMessage("Se requiere una tarjeta registrada en la app para realizar la transacción");
				}
				
				//se manda la petición
				MultiValueMap<String, String> headers = new HttpHeaders();
				headers.put("Content-Type", Collections.singletonList(MediaType.APPLICATION_JSON_VALUE));
				String basic = user+":"+pass;
				headers.put("Authorization", Collections.singletonList("Basic "+Base64.getEncoder().encodeToString(basic.getBytes())));
				
				HttpEntity<SrPagoRequest> entity = new HttpEntity<SrPagoRequest>(request, headers);
				LOGGER.info("PETICION A SR PAGO CHECKOUT - {}", GSON.toJson(request));
				ResponseEntity<SrPagoResponse> responseEntity = restTemplate.exchange("http://localhost/"+url, HttpMethod.POST, entity, SrPagoResponse.class);
				
				LOGGER.info("RESPUESTA DE CHECKOUT - {}", GSON.toJson(responseEntity.getBody()));
				
				check = responseEntity.getBody();
				resp.setCheckout(check);
				resp.setCheckoutId(idBitacora.toString());
				
				LOGGER.info("Asignacion: {}", GSON.toJson(check));
				LOGGER.info("Respuesta del metodo: {}", GSON.toJson(resp));
			} else {
				resp.setCode(-100);
                resp.setMessage(idBitacora.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
            resp.setCode(-100);
            resp.setMessage(idBitacora.getMessage());
		}
		return resp;
	}
	
	
	private Authorization getIdBitacora(Integer idApp, Integer idPais, String idioma, BasePayment pago) {
		String callTransaccion = null;
		Authorization authorization = null;
		try {
			LOGGER.info("insertatransaccion_billpocket");
			LOGGER.info("ID USUARIO: " + String.valueOf(pago.getIdUser()) + " ID ESTABLECIMIENTO: "
					+ pago.getEstablecimientoId() + " ID APP: " + idApp + " ID USUARIO: " + idioma + " PROVEEDOR : "
					+ pago.getIdProveedor() + " ID INTEGRADOR: " + " 9 " + " ID PRODUCTO: " + pago.getIdProducto()
					+ " CONCEPTO: " + pago.getConcepto() + " REFERENCIA: " + pago.getReferencia() + " CARGO: "
					+ pago.getCargo() + " COMISION: " + pago.getComision() + " ID TARJETA: " + pago.getIdCard()
					+ " TIPO TARJETA: " + pago.getTipoTarjeta() + " IMEI: " + pago.getImei() + " SW: "
					+ pago.getSoftware() + " MODELO: " + pago.getModelo() + " PROPINA: " + pago.getPropina() + " LAT: "
					+ pago.getLat() + " LON: " + pago.getLon());
			if (pago.getEstablecimientoId() != 0) {
				pago.setIdProveedor("49");
				pago.setConcepto("Pago - Scan & Pay - Establecimiento -  " + pago.getEstablecimientoId());
				pago.setReferencia("");
				pago.setEmisor("");
			} else if (pago.getIdProduct() == 48) {
				pago.setIdProveedor("48");
				pago.setIdProducto(48);
				pago.setConcepto(pago.getConcept());
			}
			pago.setCargo(Double.valueOf(pago.getCargo()));
			pago.setComision(Double.valueOf(pago.getComision()));
			LOGGER.info("NUEVO VALOR DE MONTO: {}", pago.getCargo());
			LOGGER.info("NUEVO VALOR DE COMISION: {}", pago.getComision());
			if (pago.getIdCard() == 0 && pago.getIdUser() == 0) {
				callTransaccion = authorizationRepository.insertatransaccion_billpocket_tarjeta_presente(
						String.valueOf(pago.getEstablecimientoId()), idApp, idioma,
						Integer.valueOf(pago.getIdProveedor()), 9, pago.getIdProducto(), pago.getConcepto(),
						pago.getReferencia(), pago.getCargo(), pago.getComision(), pago.getIdCard(),
						pago.getTipoTarjeta(), pago.getImei(), pago.getSoftware(), pago.getModelo(), pago.getLat(),
						pago.getLon(), null, pago.getTarjetaCompra(), pago.getOperacion(), pago.getEmisor(),
						pago.getEmail(), String.valueOf(pago.getEstablecimientoId()));
			} else {
				callTransaccion = authorizationRepository.insertatransaccion_billpocket(
						String.valueOf(pago.getIdUser()), idApp, idioma, Integer.valueOf(pago.getIdProveedor()), 9,
						pago.getIdProducto(), pago.getConcepto(), pago.getReferencia(), pago.getCargo(),
						pago.getComision(), pago.getIdCard(), pago.getTipoTarjeta(), pago.getImei(), pago.getSoftware(),
						pago.getModelo(), pago.getLat(), pago.getLon(), null, pago.getTarjetaCompra(),
						pago.getOperacion(), pago.getEmisor(), String.valueOf(pago.getEstablecimientoId()));
			}
			LOGGER.info("RESPUESTA DE INSERTA TRANSACCION: {}", callTransaccion);
			authorization = GSON.fromJson(callTransaccion, Authorization.class);
			if (authorization.getIdTransaccion() == 0) {
				authorization.setIdBitacora(0);
				authorization.setCode(-100);
			}
		} catch (Exception e) {
			e.printStackTrace();
			authorization.setCode(-100);
			authorization.setMessage("Disculpe las molestias. No fue posible procesar su solicitud. El Banco no pudo comenzar su autorizacion.");
		}
		return authorization;
	}

}
