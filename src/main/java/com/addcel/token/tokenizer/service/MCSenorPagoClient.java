package com.addcel.token.tokenizer.service;

import com.addcel.token.tokenizer.bean.BasePayment;
import com.addcel.token.tokenizer.bean.SrPagoCheckout;
import com.addcel.token.tokenizer.model.domain.Tarjetas;

public interface MCSenorPagoClient {

	public SrPagoCheckout checkOut(Integer idApp, Integer idPais, String idioma, BasePayment pago, Tarjetas tarjeta);
	
}
