package com.addcel.token.tokenizer.service;

import com.addcel.token.tokenizer.bean.ThreatMetrixRequest;
import com.addcel.token.tokenizer.bean.ThreatMetrixResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import java.util.Collections;

@Service
public class ThreatMetrixClientImpl implements ThreatMetrixClient {
	
	private static final String THREATHMETRIX_URL_PAYMENT = "http://localhost/ThreatMetrix/api/investigarPago";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ThreatMetrixClientImpl.class);
	
	private Gson GSON = new Gson();

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public ThreatMetrixResponse investigatePayment(ThreatMetrixRequest request) {
		ThreatMetrixResponse validate = null;
		ResponseEntity<String> responseEntity = null;
		try {
			RestTemplate rest = new RestTemplate();
			MultiValueMap<String, String> headers = new HttpHeaders();
			headers.put("Accept", Collections.singletonList(MediaType.APPLICATION_JSON_VALUE));
			headers.put("Content-Type", Collections.singletonList(MediaType.APPLICATION_JSON_VALUE));
			headers.put("Authorization", Collections.singletonList("Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx"));
			HttpEntity<ThreatMetrixRequest> entity = new HttpEntity<ThreatMetrixRequest>(request, headers);
			LOGGER.info("PETICION A TMX  - {}", GSON.toJson(request));
			responseEntity = restTemplate.exchange(THREATHMETRIX_URL_PAYMENT, HttpMethod.POST, entity, String.class);
			LOGGER.info("RESPUESTA DE TMX - {}", responseEntity.getBody());
			validate = GSON.fromJson(responseEntity.getBody(), ThreatMetrixResponse.class);
		} catch (RestClientException ex) {
			ex.printStackTrace();
			validate = new ThreatMetrixResponse();
			validate.setCode(-250);
			validate.setMessage("Su transaccion no ha podido ser analizado para procesar su pago.");
		} catch (Exception e) {
			e.printStackTrace();
			validate = new ThreatMetrixResponse();
			validate.setCode(-250);
			validate.setMessage("Su transaccion no ha podido ser analizado para procesar su pago.");
		}
		return validate;
	}

}
