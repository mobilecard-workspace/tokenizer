package com.addcel.token.tokenizer.service;


import com.addcel.token.tokenizer.bean.ThreatMetrixRequest;
import com.addcel.token.tokenizer.bean.ThreatMetrixResponse;

public interface ThreatMetrixClient {

	public ThreatMetrixResponse investigatePayment(ThreatMetrixRequest request);
	
}