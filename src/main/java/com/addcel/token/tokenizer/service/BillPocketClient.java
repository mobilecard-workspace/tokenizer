package com.addcel.token.tokenizer.service;

import com.addcel.token.tokenizer.bean.BasePayment;
import com.addcel.token.tokenizer.bean.CheckoutResponse;

interface BillPocketClient {

    CheckoutResponse checkOut(Integer idApp,
                              Integer idPais,
                              String idioma,
                              BasePayment pago);
}
