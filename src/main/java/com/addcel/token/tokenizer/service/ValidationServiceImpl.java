package com.addcel.token.tokenizer.service;


import com.addcel.token.tokenizer.model.repository.AppAuthorizationRepository;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidationServiceImpl implements ValidationService{

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationServiceImpl.class);

    private Gson GSON = new Gson();

    @Autowired
    private AppAuthorizationRepository repository;


    @Override
    public boolean validateApplication(Integer idApp) {
        int validate = 0;
        try{
            validate = repository.validateApp(idApp, "T");
            LOGGER.info("VALIDATOR - VALIDANDO ID APP - {} - {}", idApp, validate);
            if(validate != 0){
                return true;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
