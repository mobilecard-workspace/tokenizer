package com.addcel.token.tokenizer.service;


import com.addcel.token.tokenizer.bean.BasePayment;
import com.addcel.token.tokenizer.bean.ThreatMetrixResponse;

public interface ThreatMetrixService {

    ThreatMetrixResponse validatePayment(BasePayment payment, String sessionId);

}
