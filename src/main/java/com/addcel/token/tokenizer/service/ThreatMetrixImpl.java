package com.addcel.token.tokenizer.service;

import com.addcel.token.tokenizer.bean.BasePayment;
import com.addcel.token.tokenizer.bean.ThreatMetrixRequest;
import com.addcel.token.tokenizer.bean.ThreatMetrixResponse;
import com.addcel.token.tokenizer.model.domain.Tarjetas;
import com.addcel.token.tokenizer.model.domain.Usuario;
import com.addcel.token.tokenizer.model.repository.TarjetasRepository;
import com.addcel.token.tokenizer.model.repository.UsuarioRepository;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ThreatMetrixImpl implements ThreatMetrixService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThreatMetrixImpl.class);

    @Autowired
    private UsuarioRepository userRepository;

    @Autowired
    private TarjetasRepository cardRepository;

    @Autowired
    private ThreatMetrixClient tmxClient;

    private Gson GSON = new Gson();

    @Override
    public ThreatMetrixResponse validatePayment(BasePayment payment, String profileId) {
        ThreatMetrixRequest request = new ThreatMetrixRequest();
        ThreatMetrixResponse validate = new ThreatMetrixResponse();
        Optional<Usuario> usuario = null;
        Optional<Tarjetas> card = null;
        try {
            usuario = userRepository.findByIdUsuario(payment.getIdUser());
            if(usuario.isPresent()){
                card = cardRepository.findByIdTarjeta(payment.getIdCard());
                if(card.isPresent()){
                    LOGGER.info("DATOS  DE USUARIO: "+usuario.get().getLogin());
                    request.setApplicationName("mobilecardmx");
                    request.setCantidad(payment.getCargo());
                    request.setCardNumber(AddcelCrypto.decryptTarjeta(card.get().getPan()).substring(0, 6));
                    request.setCorreo(usuario.get().getEmail());
                    request.setNombre(usuario.get().getNombre() + " " +usuario.get().getApellidoP());
                    request.setSessionId(profileId);
                    request.setTelefono(usuario.get().getNumCelular());
                    request.setUsername(usuario.get().getLogin());
                    validate = tmxClient.investigatePayment(request);
                } else {
                    validate.setCode(5500);
                    validate.setMessage("No se encontro la tarjeta indicada.");
                }
            } else {
                validate.setCode(550);
                validate.setMessage("No se encontro el usuario indicado.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            validate.setCode(500);
            validate.setMessage("No se puedo realizar la validacion, intente de nuevo. Codigo error: TK500");
        }
        LOGGER.info("RESPONSE VALIDATE USER THREATMETRIX - {}", GSON.toJson(validate));
        return validate;
    }
}
