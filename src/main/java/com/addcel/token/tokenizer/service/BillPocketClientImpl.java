package com.addcel.token.tokenizer.service;

import com.addcel.token.tokenizer.bean.*;
import com.addcel.token.tokenizer.model.repository.AuthorizationRepository;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Service
public class BillPocketClientImpl implements BillPocketClient{

    private static final Logger LOGGER = LoggerFactory.getLogger(BillPocketClientImpl.class);

    @Autowired
    private RestTemplate restTemplate;

    //PROD
    public static final String URL_CHECKOUT = "https://paywith.billpocket.com/";

    //PROD
    private static final String API_KEY = "R4TEKWZ-DNC4C9G-HMEVK7D-4SZ5D78";

    //QA
    //public static final String URL_CHECKOUT = "https://test.paywith.billpocket.com/";

    //private static final String API_KEY = "RGD9XNN-P3V4S3N-K9Z4ACE-SNKW64K";

    private Gson GSON = new Gson();

    @Autowired
    private AuthorizationRepository authorizationRepository;

    @Override
    public CheckoutResponse checkOut(Integer idApp,
                           Integer idPais,
                           String idioma,
                           BasePayment pago) {
        CheckoutResponse resp = new CheckoutResponse();
        Checkout request = new Checkout();
        ResponseEntity<CheckoutResponse> responseEntity = null;
        Authorization idBitacora = null;
        double total = 0;
        double cargo = 0;
        double comision = 0;
        double propina = 0;
        double amount = 0;
        DecimalFormat df = new DecimalFormat("#.00");
        try {
            LOGGER.info("SOLICITANDO CHECKOUT PARA 3D...");
            LOGGER.info("SOLICITANDO CHECKOUT - {}", GSON.toJson(pago));
            request.setApiKey(API_KEY);
            LOGGER.info("CARGO - COMISION  - PROPINA - {} - {} - {}", df.format(pago.getCargo()).replace(",", "."),
                    df.format(pago.getComision()).replace(",", "."),
                    df.format(pago.getPropina()).replace(",", "."));
            cargo = Double.valueOf(df.format(pago.getCargo()).replace(",", "."));
            LOGGER.info("CARGO - {}", cargo);
            comision = Double.valueOf(df.format(pago.getComision()).replace(",", "."));
            LOGGER.info("COMISION - {}", comision);
            propina = Double.valueOf(df.format(pago.getPropina()).replace(",", "."));
            LOGGER.info("PROPINA - {}", propina);
            if(pago.getCargo() == 0){
                amount = Double.valueOf(df.format(pago.getAmount()).replace(",", "."));
                pago.setCargo(amount);
                cargo = amount;
            }
            total = cargo + comision + propina;
            LOGGER.info("CALCULANDO MONTO A PAGAR  {}", total);
            request.setTotal(total);
            //request.setTotal(Double.valueOf(String.format("%.2f", request.getTotal())));
            idBitacora = getIdBitacora(idApp, idPais, idioma, pago);
            LOGGER.info("GET ID BITACORA RESPONSE: {}", GSON.toJson(idBitacora));
            if(idBitacora.getCode() == 0){
                request.setExternalId(String.valueOf(idBitacora.getIdTransaccion()));
                List<String> items = new LinkedList<String>();
                items.add(pago.getConcepto());
                request.setItems(items);
                RestTemplate rest = new RestTemplate();
                MultiValueMap<String, String> headers = new HttpHeaders();
                headers.put("Content-Type", Collections.singletonList(MediaType.APPLICATION_JSON_VALUE));
                HttpEntity<Checkout> entity = new HttpEntity<Checkout>(request, headers);
                LOGGER.info("PETICION A BILLPOCKET CHECKOUT - {}", GSON.toJson(request));
                responseEntity = restTemplate.exchange(URL_CHECKOUT+"api/v1/checkout", HttpMethod.POST, entity,
                        CheckoutResponse.class);
                resp = responseEntity.getBody();
                resp.setCheckoutId(URL_CHECKOUT+"checkout/"+resp.getCheckoutId());
                LOGGER.info("RESPUESTA DE CHECKOUT - {}", GSON.toJson(resp));
            } else {
                resp.setCode(-100);
                resp.setMessage(idBitacora.getMessage());
            }
        } catch (Exception e){
            e.printStackTrace();
            resp.setCode(-100);
            resp.setMessage(idBitacora.getMessage());
        }
        return resp;
    }

    private Authorization getIdBitacora(Integer idApp,
                                 Integer idPais,
                                 String idioma,
                                 BasePayment pago) {
        String callTransaccion = null;
        Authorization authorization = null;
        try {
            LOGGER.info("insertatransaccion_billpocket");
            LOGGER.info("ID USUARIO: "+String.valueOf(pago.getIdUser())+
                    " ID ESTABLECIMIENTO: "+pago.getEstablecimientoId()+
                    " ID APP: "+idApp+
                    " ID USUARIO: "+idioma+
                    " PROVEEDOR : "+pago.getIdProveedor()+
                    " ID INTEGRADOR: "+
                    " 9 "+
                    " ID PRODUCTO: "+ pago.getIdProducto()+
                    " CONCEPTO: "+pago.getConcepto()+
                    " REFERENCIA: "+ pago.getReferencia()+
                    " CARGO: "+pago.getCargo()+
                    " COMISION: "+pago.getComision()+
                    " ID TARJETA: "+pago.getIdCard()+
                    " TIPO TARJETA: "+pago.getTipoTarjeta()+
                    " IMEI: "+pago.getImei()+
                    " SW: "+pago.getSoftware()+
                    " MODELO: "+pago.getModelo()+
                    " PROPINA: "+pago.getPropina()+
                    " LAT: "+pago.getLat()+
                    " LON: "+pago.getLon());
            if(pago.getEstablecimientoId() != 0){
                pago.setIdProveedor("49");
                pago.setConcepto("Pago - Scan & Pay - Establecimiento -  "+pago.getEstablecimientoId());
                pago.setReferencia("");
                pago.setEmisor("");
            } else if (pago.getIdProduct() == 48){
                pago.setIdProveedor("48");
                pago.setIdProducto(48);
                pago.setConcepto(pago.getConcept());
            }
            pago.setCargo(Double.valueOf(pago.getCargo()));
            pago.setComision(Double.valueOf(pago.getComision()));
            LOGGER.info("NUEVO VALOR DE MONTO: {}", pago.getCargo());
            LOGGER.info("NUEVO VALOR DE COMISION: {}", pago.getComision());
            if(pago.getIdCard() == 0 && pago.getIdUser() == 0 ){
                callTransaccion = authorizationRepository.insertatransaccion_billpocket_tarjeta_presente(String.valueOf(pago.getEstablecimientoId()),
                        idApp, idioma, Integer.valueOf(pago.getIdProveedor()), 9, pago.getIdProducto(),
                        pago.getConcepto(), pago.getReferencia(), pago.getCargo(),
                        pago.getComision(), pago.getIdCard(), pago.getTipoTarjeta(),
                        pago.getImei(), pago.getSoftware(), pago.getModelo(), pago.getLat(),
                        pago.getLon(), null, pago.getTarjetaCompra(), pago.getOperacion(), pago.getEmisor(), pago.getEmail(),
                        String.valueOf(pago.getEstablecimientoId()));
            } else {
                callTransaccion = authorizationRepository.insertatransaccion_billpocket(String.valueOf(pago.getIdUser()),
                        idApp, idioma, Integer.valueOf(pago.getIdProveedor()), 9, pago.getIdProducto(),
                        pago.getConcepto(), pago.getReferencia(), pago.getCargo(),
                        pago.getComision(), pago.getIdCard(), pago.getTipoTarjeta(),
                        pago.getImei(), pago.getSoftware(), pago.getModelo(), pago.getLat(),
                        pago.getLon(), null, pago.getTarjetaCompra(), pago.getOperacion(), pago.getEmisor(),
                        String.valueOf(pago.getEstablecimientoId()));
            }
            LOGGER.info("RESPUESTA DE INSERTA TRANSACCION: {}", callTransaccion);
            authorization = GSON.fromJson(callTransaccion, Authorization.class);
            if(authorization.getIdTransaccion() == 0){
                authorization.setIdBitacora(0);
                authorization.setCode(-100);
            }
        } catch(Exception e){
            e.printStackTrace();
            authorization.setCode(-100);
            authorization.setMessage("Disculpe las molestias. No fue posible procesar su solicitud. El Banco no pudo comenzar su autorizacion.");
        }
        return authorization;
    }


}
