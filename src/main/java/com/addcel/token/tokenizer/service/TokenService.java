package com.addcel.token.tokenizer.service;


import com.addcel.token.tokenizer.bean.BaseResponse;
import com.addcel.token.tokenizer.bean.Token;
import com.addcel.token.tokenizer.bean.TokenSrPago;

public interface TokenService {

    Token generateToken(Integer idApp, Integer idPais, String idioma, String authorization, String profile,
                        String sessionId, String ip);
	
	TokenSrPago generateTokenSrPago(Integer idApp, Integer idPais, String idioma, String authorization, String profile,
            String sessionId, String ip);

    BaseResponse validateToken(Integer idApp, Integer idPais, String idioma, String header, Token token,
                               String id, String remoteAddr);

    public BaseResponse validateWhiteList(long idUsuario);
}