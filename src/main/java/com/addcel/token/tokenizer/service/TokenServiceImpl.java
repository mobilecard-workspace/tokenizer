package com.addcel.token.tokenizer.service;


import com.addcel.token.tokenizer.bean.*;
import com.addcel.token.tokenizer.bean.srpago.SrPagoResponse;
import com.addcel.token.tokenizer.model.domain.Antad;
import com.addcel.token.tokenizer.model.domain.AppKeyCipher;
import com.addcel.token.tokenizer.model.domain.Tarjetas;
import com.addcel.token.tokenizer.model.domain.WhiteListUser;
import com.addcel.token.tokenizer.model.repository.AntadRepository;
import com.addcel.token.tokenizer.model.repository.CipherRepository;
import com.addcel.token.tokenizer.model.repository.EmailRepository;
import com.addcel.token.tokenizer.model.repository.TarjetasRepository;
import com.addcel.token.tokenizer.model.repository.WhiteListRepository;
import com.addcel.token.tokenizer.utils.Utils;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TokenServiceImpl implements TokenService{

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenServiceImpl.class);

    private Gson GSON = new Gson();

    @Autowired
    private CipherRepository repository;

    @Autowired
    private BillPocketClientImpl bpService;
    
    @Autowired
    private MCSenorPagoClient senorPagoClient;

    @Autowired
    private WhiteListRepository wlRepository;

    @Autowired
    private TarjetasRepository tarjetasRepository;

    @Autowired
    private AntadRepository antadRepository;
    
    @Autowired
    private EmailRepository emailRepository;

    @Override
    public Token generateToken(Integer idApp,
                               Integer idPais,
                               String idioma,
                               String authorization,
                               String profileId,
                               String sessionId,
                               String ip) {
        Token response = new Token();
        Optional<AppKeyCipher> cipher = null;
        String json = null;
        BasePayment pago = null;
        CheckoutResponse checkout = null;
        SecurePaths paths = null;
        Optional<WhiteListUser> whiteListUser = null;
        Optional<Tarjetas> tarjeta = null;
        Optional<List<Antad>> antad = null;
        boolean validateCard = false;
        try {
            LOGGER.info("JSON: {}", authorization);
            if (idApp == 1) {
                json = Utils.decryptJson(authorization);
            } else if(idApp == 4 || idApp == 5) {
                cipher = repository.findById(idApp);
                LOGGER.info("ID APP - {} - KEY - {}", idApp, cipher.get().getApiKey());
                LOGGER.info("DATA - {}", authorization);

                if(cipher.get().getApiKey() != null) {
                    json = Utils.decryptJson(authorization, cipher.get().getApiKey());
                    LOGGER.info("ID APP - {} - DATA - {}", idApp, json);
                } else {
                    response.setMessage("Peticion no valida. Llave no permitida.");
                    response.setCode(600);
                }
            } else {
                cipher = repository.findById(idApp);
                if (cipher.get().getApiKey() != null) {
                    json = Utils.decryptJson(authorization, cipher.get().getApiKey());
                } else {
                    response.setMessage("Peticion no valida. Llave no permitida.");
                    response.setCode(600);
                }
            }
            LOGGER.info("JSON DECRYPT: {}", json);
            pago = (BasePayment) GSON.fromJson(json, BasePayment.class);
            if (pago.getEstablecimientoId() == 0) {
                if(pago.getIdUser() == 0){
                    pago.setIdUser(pago.getIdUsuario());
                }
                response = generaToken(pago.getIdUser(), pago.getIdCard(), sessionId, ip);
                /*validate = tmxService.validatePayment(pago, profileId);
                if (validate.getCode() == 0) {
                    if ("high".equals(validate.getTmxRisk())) {
                        response.setMessage("Se ha detectado actividad inusual en la transaccion. No se puede realizar el pago.");
                        response.setCode(900);
                    } else {
                        response = generaToken(pago.getIdUser(), pago.getIdCard(), sessionId, ip);
                        if ("neutral".equals(validate.getTmxRisk())) {
                            response.setSecure(false);
                        }
                    }
                } else {
                    response.setMessage(validate.getMessage());
                    response.setCode(910);
                }*/
            } else {
                pago = (BasePayment) GSON.fromJson(json, BasePayment.class);
                response = generaToken(pago.getEstablecimientoId(), pago.getIdCard(), sessionId, ip);
            }
            LOGGER.info("ID PAIS: {}", idPais);
            if(idPais == 1){
                if(pago.getIdProveedor() == null){
                    pago.setIdProveedor("49");
                }
                LOGGER.info("VALIDANDO SI ES UN PAGO ANTAD - {}", pago.getEmisor());
                if(StringUtils.isNotEmpty(pago.getEmisor())){
                    antad = antadRepository.findByEmisor(pago.getEmisor());
                    LOGGER.info("SE HAN ENCONTRADO EMISORES CORRESPONDIENTES EN ANTAD - {}", GSON.toJson(antad));
                    if(antad.isPresent()){
                        pago.setIdProveedor("45");
                        pago.setIdProducto(Integer.valueOf(pago.getEmisor()));
                    }
                }

                LOGGER.info("VALIDANDO ID PROVEEDOR: {}", pago.getIdProveedor());
                if(pago.getIdCard() != 0){
                    tarjeta = tarjetasRepository.findByIdTarjeta(pago.getIdCard());
                    LOGGER.info("VALIDANDO EL TIPO DE TARJETA - ID CARD - {} - {}", tarjeta.get().getFranquicia());
                    if("3".equals(tarjeta.get().getFranquicia())){
                        validateCard = true;
                    }
                    pago.setTarjetaCompra(tarjeta.get().getPan());
                } else {
                    LOGGER.info("VALIDANDO SI ES UNA TARJETA AMEX...");
                    if(pago.getTarjeta() != null){
                        String cardAmex = AddcelCrypto.decryptHard(pago.getTarjeta());
                        LOGGER.info("TARJETA: {}", cardAmex);
                        if(cardAmex.startsWith("34") || cardAmex.startsWith("37")){
                            LOGGER.info("TARJETA AMEX...");
                            response.setSecure(false);
                            validateCard = true;
                        }
                    }
                }

                if("800".equals(pago.getIdProveedor()) || validateCard ){
                    response.setSecure(false);
                } else {
                    checkout = bpService.checkOut(idApp, idPais, idioma, pago);
                    if (checkout.getCode() == 0) {
                        response.setSecure(true);
                        paths = new SecurePaths();
                        paths.setForm(checkout.getCheckoutId());
                        paths.setAuthError("https://paywith.billpocket.com/checkout/error");
                        paths.setStart(checkout.getCheckoutId());
                        paths.setSuccess("https://www.mobilecard.mx/BillPocket/3d/response");
                        paths.setError("https://www.mobilecard.mx/BillPocket/3d/error");
                        response.setPaths(paths);
                        response.setPath3D(checkout.getCheckoutId());
                    } else {
                        response.setCode(-1000);
                        response.setMessage(checkout.getMessage());
                    }
                    response.setSecure(true);
                }
            }
            whiteListUser = wlRepository.findByIdUsuarioAndStatus(pago.getIdUser(), "1");
                if(whiteListUser.isPresent()){
                LOGGER.info("USUARIO ESTA EN WHITE LIST...");
                response.setWhiteList(true);
            }
            if(pago.getEstablecimientoId() == 148 || pago.getEstablecimientoId() == 1460
                    || pago.getEstablecimientoId() == 1434 || pago.getEstablecimientoId() == 1458
                    || pago.getEstablecimientoId() == 1462){
                response.setSecure(false);
            }
            response.setJson("");
        } catch (Exception e){
            e.printStackTrace();
            response.setMessage("No fue posible consultar el token en BD");
            response.setCode(-1);
        }
        LOGGER.info("ENVIANDO RESPUESTA DE GET TOKEN: {}", GSON.toJson(response));
        return response;
    }
    
    @Override
    public TokenSrPago generateTokenSrPago(Integer idApp, Integer idPais, String idioma, String authorization, String profileId, String sessionId, String ip) {
    	TokenSrPago response = new TokenSrPago();
    	Optional<AppKeyCipher> cipher = null;
        String json = null;
        BasePayment pago = null;
        SrPagoCheckout checkout = null;
        Optional<WhiteListUser> whiteListUser = null;
        Optional<Tarjetas> tarjeta = null;
        Optional<List<Antad>> antad = null;
        boolean validateCard = false;
        SecurePathsSrPago paths = null;
        SrPagoResponse srPagoResponse = null;
        try {
            LOGGER.info("JSON: {}", authorization);
            if (idApp == 1) {
                json = Utils.decryptJson(authorization);
            } else if(idApp == 4 || idApp == 5) {
                cipher = repository.findById(idApp);
                LOGGER.info("ID APP - {} - KEY - {}", idApp, cipher.get().getApiKey());
                LOGGER.info("DATA - {}", authorization);

                if(cipher.get().getApiKey() != null) {
                    json = Utils.decryptJson(authorization, cipher.get().getApiKey());
                    LOGGER.info("ID APP - {} - DATA - {}", idApp, json);
                } else {
                    response.setMessage("Peticion no valida. Llave no permitida.");
                    response.setCode(600);
                }
            } else {
                cipher = repository.findById(idApp);
                if (cipher.get().getApiKey() != null) {
                    json = Utils.decryptJson(authorization, cipher.get().getApiKey());
                } else {
                    response.setMessage("Peticion no valida. Llave no permitida.");
                    response.setCode(600);
                }
            }
            LOGGER.info("JSON DECRYPT: {}", json);
            pago = (BasePayment) GSON.fromJson(json, BasePayment.class);
            if (pago.getEstablecimientoId() == 0) {
                if(pago.getIdUser() == 0){
                    pago.setIdUser(pago.getIdUsuario());
                }
                response = generaTokenSrPago(pago.getIdUser(), pago.getIdCard(), sessionId, ip);
            } else {
                pago = (BasePayment) GSON.fromJson(json, BasePayment.class);
                response = generaTokenSrPago(pago.getEstablecimientoId(), pago.getIdCard(), sessionId, ip);
            }
            LOGGER.info("ID PAIS: {}", idPais);
            if(idPais == 1){
                if(pago.getIdProveedor() == null){
                    pago.setIdProveedor("49");
                }
                LOGGER.info("VALIDANDO SI ES UN PAGO ANTAD - {}", pago.getEmisor());
                if(StringUtils.isNotEmpty(pago.getEmisor())){
                    antad = antadRepository.findByEmisor(pago.getEmisor());
                    LOGGER.info("SE HAN ENCONTRADO EMISORES CORRESPONDIENTES EN ANTAD - {}", GSON.toJson(antad));
                    if(antad.isPresent()){
                        pago.setIdProveedor("45");
                        pago.setIdProducto(Integer.valueOf(pago.getEmisor()));
                    }
                }

                LOGGER.info("VALIDANDO ID PROVEEDOR: {}", pago.getIdProveedor());
                if(pago.getIdCard() != 0){
                    tarjeta = tarjetasRepository.findByIdTarjeta(pago.getIdCard());
                    LOGGER.info("VALIDANDO EL TIPO DE TARJETA - ID CARD - {} - {}", tarjeta.get().getFranquicia());
                    if("3".equals(tarjeta.get().getFranquicia())){
                        validateCard = true;
                    }
                    pago.setTarjetaCompra(tarjeta.get().getPan());
                } else {
                    LOGGER.info("VALIDANDO SI ES UNA TARJETA AMEX...");
                    if(pago.getTarjeta() != null){
                        String cardAmex = AddcelCrypto.decryptHard(pago.getTarjeta());
                        LOGGER.info("TARJETA: {}", cardAmex);
                        if(cardAmex.startsWith("34") || cardAmex.startsWith("37")){
                            LOGGER.info("TARJETA AMEX...");
                            response.setSecure(false);
                            validateCard = true;
                        }
                    }
                }

                if("800".equals(pago.getIdProveedor()) || validateCard ){
                    response.setSecure(false);
                } else {
                    checkout = senorPagoClient.checkOut(idApp, idPais, idioma, pago, tarjeta.get());
                    if (checkout.getCode() == 0) {
                        response.setSecure(true);
                        //codigo sr pago
                        String htmlTemplate = emailRepository.findHtml("@TEMPLATE_HTML");
                        paths = new SecurePathsSrPago();
                        srPagoResponse = checkout.getCheckout();
                        paths.setForm(htmlTemplate.replace("${form}", srPagoResponse.getData().getResult().getForm()));
                        paths.setParameters(srPagoResponse.getData().getResult().getParameters());
                        paths.setSuccess(srPagoResponse.getSuccess());
                        paths.setToken(srPagoResponse.getData().getResult().getToken());
                        paths.setUrl(srPagoResponse.getData().getResult().getUrl());
                        LOGGER.info("Paths Sr Pago: {}", GSON.toJson(paths));
                        response.setPaths(paths);
                        response.setPath3D(srPagoResponse.getData().getResult().getUrl());
                    } else {
                        response.setCode(-1000);
                        response.setMessage(checkout.getMessage());
                    }
                    response.setSecure(true);
                }
            }
            whiteListUser = wlRepository.findByIdUsuarioAndStatus(pago.getIdUser(), "1");
                if(whiteListUser.isPresent()){
                LOGGER.info("USUARIO ESTA EN WHITE LIST...");
                response.setWhiteList(true);
            }
            if(pago.getEstablecimientoId() == 148 || pago.getEstablecimientoId() == 1460
                    || pago.getEstablecimientoId() == 1434 || pago.getEstablecimientoId() == 1458
                    || pago.getEstablecimientoId() == 1462){
                response.setSecure(false);
            }
            response.setJson("");
        } catch (Exception e){
            e.printStackTrace();
            response.setMessage("No fue posible consultar el token en BD");
            response.setCode(-1);
        }
        LOGGER.info("ENVIANDO RESPUESTA DE GET TOKEN: {}", GSON.toJson(response));
        return response;
    }

    private Token generaToken(long idUsuario, int idTarjeta, String sessionId, String ip) {
        String token = null;
        Token respuesta = new Token();
        try {
            token = repository.getToken();
            LOGGER.info("TOKEN BD: "+token);
            token = Utils.encryptJson(token);
            LOGGER.info("TOKEN BD: "+token);
            token = token + "#" + idUsuario + "#" + idTarjeta + "#"
                    + sessionId + "#" + ip;
            LOGGER.info("TOKEN GENERADO: "+token);
            token = Utils.encryptJson(token);
            LOGGER.info("TOKEN GENERADO: "+token);
            respuesta.setAccountId(Utils.digest(idUsuario));
            respuesta.setToken(token);
            respuesta.setMessage("Success");

        } catch (Exception e) {
            e.printStackTrace();
            respuesta.setMessage("No fue posible consultar el token en BD");
            respuesta.setCode(-1);
        }
        return respuesta;
    }
    
    private TokenSrPago generaTokenSrPago(long idUsuario, int idTarjeta, String sessionId, String ip) {
        String token = null;
        TokenSrPago respuesta = new TokenSrPago();
        try {
            token = repository.getToken();
            LOGGER.info("TOKEN BD: "+token);
            token = Utils.encryptJson(token);
            LOGGER.info("TOKEN BD: "+token);
            token = token + "#" + idUsuario + "#" + idTarjeta + "#"
                    + sessionId + "#" + ip;
            LOGGER.info("TOKEN GENERADO: "+token);
            token = Utils.encryptJson(token);
            LOGGER.info("TOKEN GENERADO: "+token);
            respuesta.setAccountId(Utils.digest(idUsuario));
            respuesta.setToken(token);
            respuesta.setMessage("Success");

        } catch (Exception e) {
            e.printStackTrace();
            respuesta.setMessage("No fue posible consultar el token en BD");
            respuesta.setCode(-1);
        }
        return respuesta;
    }

    @Override
    public BaseResponse validateToken(Integer idApp, Integer idPais, String idioma, String header, Token token,
                                      String id, String remoteAddr) {
        String idUsuario = null;
        String idTarjeta = null;
        String ip = null;
        String sessionId = null;
        String headerArray[] = null;
        int tokenBd = 0;
        String tokenHeader = null;
        BasePayment pago = null;
        BaseResponse response = new BaseResponse();
        Optional<AppKeyCipher> cipher = null;
        try {
            if(header != null) {
                header = Utils.decryptJson(header);
                LOGGER.info("HEADER: {} - IP - {}", header, remoteAddr + " - SESSION ID  - "+id);
                if(idApp == 4 || idApp == 5) {
                    cipher = repository.findById(idApp);
                    LOGGER.info("ID APP - {} - KEY - {}", idApp, cipher.get().getApiKey());
                    LOGGER.info("DATA - {}", token.getJson());
                    if(cipher.get().getApiKey() != null) {
                        token.setJson(Utils.decryptJson(token.getJson(), cipher.get().getApiKey()));
                        LOGGER.info("ID APP - {} - DATA - {}", idApp, token.getJson());
                    } else {
                        response.setCode(-1);
                        response.setMessage("No hay key configurado para ese producto. ");
                    }
                } else {
                    token.setJson(Utils.decryptJson(token.getJson()));
                }
                pago = GSON.fromJson(token.getJson(), BasePayment.class);
                LOGGER.info("JSON: {}", token.getJson());
                LOGGER.info("VALIDANDO TOKEN AUTHORIZATION...");

                if(!token.getAccountId().equals(Utils.digest(pago.getIdUser()))) {
                    response.setCode(-1);
                    response.setMessage("El identificador de la cuenta no corresponde con el usuario.");
                }
                headerArray = header.split("#");
                if(headerArray.length == 5) {
                    LOGGER.info("VALIDANDO TIME STAMP: {}", token.getToken());
                    tokenHeader = headerArray[0];
                    tokenHeader = AddcelCrypto.decryptSensitive(tokenHeader);
                    LOGGER.info("TOKEN: {}", tokenHeader);
                    tokenBd = repository.difFechaMin(tokenHeader);
                    LOGGER.info("VALIDANDO TIME STAMP - OK - H: {} - BD:{}", tokenHeader, tokenBd);
                    if(tokenBd < 30000) {
                        idUsuario = headerArray[1];
                        if (pago.getEstablecimientoId() != 0) {
                            response.setCode(0);
                            response.setMessage("Validate Success");
                        } else {
                            LOGGER.info("VALIDANDO USUARIO  - H:{} - BD:{}", idUsuario, pago.getIdUser());
                            if (idUsuario.equals(String.valueOf(pago.getIdUser()))) {
                                LOGGER.info("VALIDANDO USUARIO - OK - H: {} - P: {}", idUsuario, pago.getIdUser());
                                idTarjeta = headerArray[2];
                                if (idTarjeta.equals(String.valueOf(pago.getIdCard()))) {
                                    LOGGER.info("VALIDANDO ID TARJETA - OK - H: {} - P: {}", idTarjeta, pago.getIdCard());
                                    ip = headerArray[4];
                                    response.setCode(0);
                                    response.setMessage("Validate Success");
                                    /**if(ip.equals(remoteAddr)) {
                                     LOGGER.info("VALIDANDO IP - OK - H: {} - P: {}", ip, remoteAddr);
                                     response.setCode(0);
                                     response.setMessage("Validate Success");
                                     } else {
                                     LOGGER.info("ERROR AL VALIDAR IP - NO COINCIDEN - H: {} - BD:{}", ip, remoteAddr);
                                     }*/
                                } else {
                                    LOGGER.info("ERROR AL VALIDAR TARJETAS  - NO COINCIDEN - H: {} - BD:{}", idTarjeta, pago.getIdCard());
                                }
                            } else {
                                LOGGER.info("ERROR AL VALIDAR USUARIO  - NO COINCIDEN - H: {} - BD:{}", idUsuario, pago.getIdUser());
                            }
                        }
                    } else {
                        LOGGER.info("ERROR AL VALIDAR TIME STAMP  - EXCEDIO TIEMPO LIMITE - H: {} - BD:{}", token, tokenBd);
                    }
                } else {
                    LOGGER.info("ERROR AL VALIDAR HEADER  - DATOS INCOMPLETOS - {}", header);
                }
            }else {
                LOGGER.info("ERROR AL VALIDAR HEADER  - NO HEADER - {}", header);
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("ERROR AL CONSULTAR EL TOKEN EN BD - CAUSA: ["+e.getCause()+"]");
            response.setCode(-1);
            response.setMessage("No se pudo realizar la validacion del token.");
        }
        return response;
    }

    public BaseResponse validateWhiteList(long idUsuario){
        BaseResponse resp = new BaseResponse();
        Optional<WhiteListUser> whiteListUser = null;
        try {
            /*whiteListUser = wlRepository.findByIdUsuarioAndStatus(idUsuario, "1");
            if(whiteListUser.isPresent()){
                LOGGER.info("USUARIO WHITE LIST");
                resp.setCode(0);
                resp.setMessage("Usuario existente.");
            } else {
                LOGGER.info("USUARIO NO WHITE LIST");
                resp.setCode(-1);
                resp.setMessage("Usuario no existente.");
            }*/
            resp.setCode(0);
            resp.setMessage("3D Secure Activado");
        } catch (Exception e){
            e.printStackTrace();
            LOGGER.info("USUARIO NO WHITE LIST");
            resp.setCode(-1);
            resp.setMessage("Usuario no existente.");
        }
        return resp;
    }

}
