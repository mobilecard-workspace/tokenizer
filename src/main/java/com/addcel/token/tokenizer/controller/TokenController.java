package com.addcel.token.tokenizer.controller;

import com.addcel.token.tokenizer.bean.BaseResponse;
import com.addcel.token.tokenizer.bean.Token;
import com.addcel.token.tokenizer.service.TokenService;
import com.addcel.token.tokenizer.service.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
public class TokenController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenController.class);

    private static final String PATH_GET_TOKEN = "/{idApp}/{idPais}/{idioma}/getToken";

    private static final String PATH_VALIDATE_TOKEN = "/{idApp}/{idPais}/{idioma}/validateToken";

    private static final String PATH_WHITE_LIST = "/{idApp}/{idPais}/{idioma}/whiteList";

    private static final String HEADER_AUTH = "Authorization";

    private static final String HEADER_PROFILE = "Profile";

    @Autowired
    private TokenService service;

    @Autowired
    private ValidationService validationService;

    @RequestMapping(value = PATH_GET_TOKEN, method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<BaseResponse> getToken(@PathVariable Integer idApp,
                                                 @PathVariable Integer idPais,
                                                 @PathVariable String idioma,
                                                 HttpServletRequest req) {
        LOGGER.info("SOLICITANDO TOKEN - AUTHORIZATION - {} - PROFILE - {}", req.getHeader(HEADER_AUTH),
                req.getHeader(HEADER_PROFILE));
        try {
            if(validationService.validateApplication(idApp)){
                if(req.getHeader(HEADER_AUTH).isEmpty()) {
                    return new ResponseEntity<BaseResponse>(new BaseResponse(-1,
                            "No estas autorizado para realizar esta operacion."), HttpStatus.UNAUTHORIZED);
                } else {
                    /*return new ResponseEntity<BaseResponse>(service.generateToken(
                            idApp,
                            idPais,
                            idioma,
                            req.getHeader(HEADER_AUTH),
                            req.getHeader(HEADER_PROFILE),
                            req.getSession().getId(),
                            req.getRemoteAddr()), HttpStatus.OK);*/
                	return new ResponseEntity<BaseResponse>(service.generateTokenSrPago(idApp, idPais, idioma, req.getHeader(HEADER_AUTH), req.getHeader(HEADER_PROFILE), req.getSession().getId(), req.getRemoteAddr()), HttpStatus.OK);
                }
            } else {
                return new ResponseEntity<BaseResponse>(new BaseResponse(-401,
                        "No estas autorizado para realizar esta operacion."), HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<BaseResponse>(new BaseResponse(-500,
                    "Error interno, favor de contactar a soporte@addcel.com"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = PATH_VALIDATE_TOKEN, method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<BaseResponse> validateToken(@PathVariable Integer idApp,
                                                 @PathVariable Integer idPais,
                                                 @PathVariable String idioma,
                                                 @RequestBody Token token,
                                                 HttpServletRequest req) {
        LOGGER.debug("VALIDANDO TOKEN...");
        try {
            if(req.getHeader(HEADER_AUTH).isEmpty()) {
                return new ResponseEntity<BaseResponse>(new BaseResponse(-1,
                        "No estas autorizado para realizar esta operacion."), HttpStatus.UNAUTHORIZED);
            } else {
                return new ResponseEntity<BaseResponse>(service.validateToken(
                        idApp,
                        idPais,
                        idioma,
                        req.getHeader(HEADER_AUTH),
                        token,
                        req.getSession().getId(),
                        req.getRemoteAddr()), HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<BaseResponse>(new BaseResponse(-1,
                    "Error interno, favor de contactar a soporte@addcel.com"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = PATH_WHITE_LIST, method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<BaseResponse> whiteList(@PathVariable Integer idApp,
                                                  @PathVariable Integer idPais,
                                                  @PathVariable String idioma,
                                                  @RequestParam long idUsuario,
                                                  HttpServletRequest req) {
        LOGGER.debug("VALIDANDO WHITE LIST...");
        try {
            return new ResponseEntity<BaseResponse>(service.validateWhiteList(idUsuario), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<BaseResponse>(new BaseResponse(-1,
                    "Error interno, favor de contactar a soporte@addcel.com"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
