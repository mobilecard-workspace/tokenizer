package com.addcel.token.tokenizer.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_usuarios")
public class Usuario {

    @Id
    @Column(name = "id_usuario")
    private long idUsuario;

    @Column(name = "usr_login")
    private String login;

    @Column(name = "usr_pwd")
    private String password;

    @Column(name = "usr_telefono")
    private String numCelular;

    @Column(name = "usr_nombre")
    private String nombre;

    @Column(name = "usr_apellido")
    private String apellidoP;

    @Column(name = "usr_materno")
    private String apellidoM;

    @Column(name = "email")
    private String email;

    @Column(name = "id_usr_status")
    private int status;

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNumCelular() {
        return numCelular;
    }

    public void setNumCelular(String numCelular) {
        this.numCelular = numCelular;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoP() {
        return apellidoP;
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = apellidoP;
    }

    public String getApellidoM() {
        return apellidoM;
    }

    public void setApellidoM(String apellidoM) {
        this.apellidoM = apellidoM;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
