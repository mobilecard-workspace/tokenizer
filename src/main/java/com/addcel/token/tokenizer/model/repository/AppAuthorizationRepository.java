package com.addcel.token.tokenizer.model.repository;

import com.addcel.token.tokenizer.model.domain.AppKeyCipher;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppAuthorizationRepository extends CrudRepository<AppKeyCipher, Integer> {

    @Query(value = "select count(*) from APP_AUTHORIZATION where id_app_auth=:idApp and activo=:activo", nativeQuery = true)
    int validateApp(@Param("idApp") Integer idApp, @Param("activo") String activo);

}
