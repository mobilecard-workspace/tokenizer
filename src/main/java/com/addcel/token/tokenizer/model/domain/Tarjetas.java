package com.addcel.token.tokenizer.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tarjetas_usuario")
public class Tarjetas {

    @Id
    @Column(name = "idtarjetasusuario")
    private Integer idTarjeta;

    @Column(name = "id_aplicacion")
    private Integer idApp;

    @Column(name = "numerotarjeta")
    private String pan;

    @Column(name = "vigencia")
    private String expDate;

    @Column(name = "ct")
    private String ct;

    @Column(name = "estado")
    private String estado;

    @Column(name = "idfranquicia")
    private String franquicia;
    
    @Column(name = "nombre_tarjeta")
    private String nombre;

    public Integer getIdTarjeta() {
        return idTarjeta;
    }

    public void setIdTarjeta(Integer idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    public Integer getIdApp() {
        return idApp;
    }

    public void setIdApp(Integer idApp) {
        this.idApp = idApp;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getCt() {
        return ct;
    }

    public void setCt(String ct) {
        this.ct = ct;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFranquicia() {
        return franquicia;
    }

    public void setFranquicia(String franquicia) {
        this.franquicia = franquicia;
    }

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
    
}
