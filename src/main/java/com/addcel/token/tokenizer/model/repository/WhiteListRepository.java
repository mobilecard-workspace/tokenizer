package com.addcel.token.tokenizer.model.repository;

import com.addcel.token.tokenizer.model.domain.WhiteListUser;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface WhiteListRepository extends CrudRepository<WhiteListUser, Long> {

    Optional<WhiteListUser> findByIdUsuarioAndStatus(Long idusuario, String status);

}

