package com.addcel.token.tokenizer.model.repository;

import com.addcel.token.tokenizer.model.domain.AppKeyCipher;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CipherRepository extends CrudRepository<AppKeyCipher, Integer> {

    Optional<AppKeyCipher> findById(Integer id);

    @Query(value = "select DATE_FORMAT(sysdate(), '%Y/%m/%d %H:%i:%s') fecha", nativeQuery = true)
    String getToken();

    @Query(value = "SELECT TIME_TO_SEC(TIMEDIFF(sysdate(), :ntoken)) seg", nativeQuery = true)
    int difFechaMin(@Param("ntoken") String token);
}
