package com.addcel.token.tokenizer.model.repository;

import com.addcel.token.tokenizer.model.domain.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    Optional<Usuario> findByIdUsuario(Long idusuario);

}

