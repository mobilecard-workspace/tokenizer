package com.addcel.token.tokenizer.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TEMPLATE_EMAIL")
public class TemplateEmail {
	
	@Id
    @Column(name = "ID")
	private String id;
	
	@Column(name = "ASUNTO")
	private String asunto;
	
	@Column(name = "CUERPO")
	private String cuerpo;
	
	@Column(name = "ID_APP")
	private Integer idApp;
	
	@Column(name = "IDIOMA")
	private String idioma;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}

	public Integer getIdApp() {
		return idApp;
	}

	public void setIdApp(Integer idApp) {
		this.idApp = idApp;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	
}
