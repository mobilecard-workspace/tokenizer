package com.addcel.token.tokenizer.model.repository;

import com.addcel.token.tokenizer.model.domain.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface AuthorizationRepository extends CrudRepository<Transaction, Long> {

    @Query(value = "SELECT insertatransaccion_billpocket_3d(:nidusuario, :nidaplicacion, :nidioma, "+
            ":nid_proveedor, :nid_integrador, :nid_producto, :nbit_concepto, :nreferencia, :nbit_cargo, :ncomision, " +
            ":nbit_card_id,"+
            ":ncardtype, :nimei, :nsoftware, :nmodelo, :nlat, :nlon, :ncardToken, :nmaskedPan, :nemisor, :noperacion, " +
            ":nidestablecimiento)", nativeQuery = true)
    String insertatransaccion_billpocket(@Param("nidusuario") String idUsuario,
                                         @Param("nidaplicacion") int idApp, @Param("nidioma") String idioma,
                                         @Param("nid_proveedor") int idProveedor, @Param("nid_integrador") int idIntegrador,
                                         @Param("nid_producto") int idProducto, @Param("nbit_concepto") String concepto,
                                         @Param("nreferencia") String referencia, @Param("nbit_cargo") double cargo,
                                         @Param("ncomision") double comision, @Param("nbit_card_id") int idTarjeta,
                                         @Param("ncardtype") String tipoTarjeta, @Param("nimei") String imei,
                                         @Param("nsoftware") String software, @Param("nmodelo") String modelo,
                                         @Param("nlat") double lat, @Param("nlon") double lon, @Param("ncardToken") String cardToken,
                                         @Param("nmaskedPan") String maskedPan, @Param("nemisor") String emisor,
                                         @Param("noperacion") String operacion, @Param("nidestablecimiento") String idEstablecimiento);

    @Query(value = "SELECT insertatransaccion_billpocket_3d_tp(:nidusuario, :nidaplicacion, :nidioma, "+
            ":nid_proveedor, :nid_integrador, :nid_producto, :nbit_concepto, :nreferencia, :nbit_cargo, :ncomision, " +
            ":nbit_card_id,"+
            ":ncardtype, :nimei, :nsoftware, :nmodelo, :nlat, :nlon, :ncardToken, :nmaskedPan, :nemisor, :noperacion, " +
            ":ndestino, :nidestablecimiento)", nativeQuery = true)
    String insertatransaccion_billpocket_tarjeta_presente(@Param("nidusuario") String idUsuario,
                                         @Param("nidaplicacion") int idApp, @Param("nidioma") String idioma,
                                         @Param("nid_proveedor") int idProveedor, @Param("nid_integrador") int idIntegrador,
                                         @Param("nid_producto") int idProducto, @Param("nbit_concepto") String concepto,
                                         @Param("nreferencia") String referencia, @Param("nbit_cargo") double cargo,
                                         @Param("ncomision") double comision, @Param("nbit_card_id") int idTarjeta,
                                         @Param("ncardtype") String tipoTarjeta, @Param("nimei") String imei,
                                         @Param("nsoftware") String software, @Param("nmodelo") String modelo,
                                         @Param("nlat") double lat, @Param("nlon") double lon, @Param("ncardToken") String cardToken,
                                         @Param("nmaskedPan") String maskedPan, @Param("nemisor") String emisor,
                                         @Param("noperacion") String operacion,  @Param("ndestino") String email,
                                         @Param("nidestablecimiento") String idEstablecimiento);


}
