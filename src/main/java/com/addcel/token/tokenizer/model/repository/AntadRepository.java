package com.addcel.token.tokenizer.model.repository;

import com.addcel.token.tokenizer.model.domain.Antad;
import com.addcel.token.tokenizer.model.domain.AppKeyCipher;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AntadRepository extends CrudRepository<Antad, Integer> {

    Optional<List<Antad>> findByEmisor(String emisor);

}
