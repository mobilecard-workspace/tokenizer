package com.addcel.token.tokenizer.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.addcel.token.tokenizer.model.domain.TemplateEmail;

@Repository
public interface EmailRepository extends JpaRepository<TemplateEmail, String> {

	@Query("SELECT t.cuerpo FROM TemplateEmail t WHERE t.id = :id")
	String findHtml(@Param("id") String id);
}
