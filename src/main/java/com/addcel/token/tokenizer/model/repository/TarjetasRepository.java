package com.addcel.token.tokenizer.model.repository;

import com.addcel.token.tokenizer.model.domain.Tarjetas;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TarjetasRepository extends CrudRepository<Tarjetas, Long> {

    Optional<Tarjetas> findByIdTarjeta(Integer idTarjeta);

}

