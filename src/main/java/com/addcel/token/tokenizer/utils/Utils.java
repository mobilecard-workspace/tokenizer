package com.addcel.token.tokenizer.utils;

import com.addcel.utils.AddcelCrypto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    public static final String FORMATO_FECHA_ENCRIPT = "ddhhmmssSSS";

    private static final SimpleDateFormat SDF = new SimpleDateFormat(FORMATO_FECHA_ENCRIPT);

    public static String encryptJson(String json){
        return AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
    }

    public static String decryptJson(String json){
        return AddcelCrypto.decryptSensitive(json);
    }

    public static String decryptJson(String json, String key){
        String decryptedText = null;
        String salt = null;
        try {
            salt = "ff39a0df";
            LOGGER.info("DATA: "+json);
            LOGGER.info("KEY: "+key);
            LOGGER.info("SALT: "+salt);
//			TextEncryptor decryptor = Encryptors.text(key, salt);
//			decryptedText = decryptor.decrypt(json);
            TextEncryptor decryptor = Encryptors.text(key, salt);
            decryptedText = decryptor.decrypt(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decryptedText;
    }

    public static String digest(long text) {
        String digest = "";
        BigInteger bigIntDgst = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(String.valueOf(text).getBytes(), 0, String.valueOf(text).length());
            bigIntDgst = new BigInteger(1, md.digest());
            digest = bigIntDgst.toString(16);
            digest = String.format("%040x", bigIntDgst);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.info("Error al encriptar - digest", e);
        }
        return digest;
    }
}
