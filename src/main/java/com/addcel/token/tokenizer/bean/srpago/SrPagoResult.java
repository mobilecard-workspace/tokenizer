package com.addcel.token.tokenizer.bean.srpago;

import java.util.List;

public class SrPagoResult {

	private String url;
	private List<SrPagoParameters> parameters;
	private String token;
	private String form;
	
	public SrPagoResult() {}
	
	public SrPagoResult(String url, List<SrPagoParameters> parameters, String token, String form) {
		super();
		this.url = url;
		this.parameters = parameters;
		this.token = token;
		this.form = form;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public List<SrPagoParameters> getParameters() {
		return parameters;
	}
	public void setParameters(List<SrPagoParameters> parameters) {
		this.parameters = parameters;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
}
