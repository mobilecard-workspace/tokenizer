package com.addcel.token.tokenizer.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TokenSrPago extends BaseResponse {

	private String token;

    private String accountId;

    private boolean secure;

    private long idUsuario;

    private String json;

    private String path3D;
    
    private boolean whiteList;
    
    private SecurePathsSrPago paths;
    
    public TokenSrPago() {}

	public TokenSrPago(String token, String accountId, boolean secure, long idUsuario, String json, String path3d,
			boolean whiteList, SecurePathsSrPago paths) {
		super();
		this.token = token;
		this.accountId = accountId;
		this.secure = secure;
		this.idUsuario = idUsuario;
		this.json = json;
		path3D = path3d;
		this.whiteList = whiteList;
		this.paths = paths;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public boolean isSecure() {
		return secure;
	}

	public void setSecure(boolean secure) {
		this.secure = secure;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getPath3D() {
		return path3D;
	}

	public void setPath3D(String path3d) {
		path3D = path3d;
	}

	public boolean isWhiteList() {
		return whiteList;
	}

	public void setWhiteList(boolean whiteList) {
		this.whiteList = whiteList;
	}

	public SecurePathsSrPago getPaths() {
		return paths;
	}

	public void setPaths(SecurePathsSrPago paths) {
		this.paths = paths;
	}
    
}
