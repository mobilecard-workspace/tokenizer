package com.addcel.token.tokenizer.bean;

import java.util.List;

import com.addcel.token.tokenizer.bean.srpago.SrPagoParameters;

public class SecurePathsSrPago {

	private Boolean success;
	
	private String form;
	
	private String url;
	
	private String token;
	
	private List<SrPagoParameters> parameters;
	
	public SecurePathsSrPago() {}

	public SecurePathsSrPago(Boolean success, String form, String url, String token, List<SrPagoParameters> parameters) {
		super();
		this.success = success;
		this.form = form;
		this.url = url;
		this.token = token;
		this.parameters = parameters;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public List<SrPagoParameters> getParameters() {
		return parameters;
	}

	public void setParameters(List<SrPagoParameters> parameters) {
		this.parameters = parameters;
	}

}
