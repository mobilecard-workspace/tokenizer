package com.addcel.token.tokenizer.bean.srpago;

public class SrPagoRequest {

	private Double amount;
	private String number;
	private String holder;
	private String month;
	private String year;
	private String reference;
	private String cvv;
	
	public SrPagoRequest() {}

	public SrPagoRequest(Double amount, String number, String holder, String month, String year, String reference,
			String cvv) {
		super();
		this.amount = amount;
		this.number = number;
		this.holder = holder;
		this.month = month;
		this.year = year;
		this.reference = reference;
		this.cvv = cvv;
	}

	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getHolder() {
		return holder;
	}
	public void setHolder(String holder) {
		this.holder = holder;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	
}
