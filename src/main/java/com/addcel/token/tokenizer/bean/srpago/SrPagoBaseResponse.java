package com.addcel.token.tokenizer.bean.srpago;

public class SrPagoBaseResponse {

	private Integer idError;
	private String mensajeError;
	
	public SrPagoBaseResponse() {}
	
	public SrPagoBaseResponse(Integer idError, String mensajeError) {
		super();
		this.idError = idError;
		this.mensajeError = mensajeError;
	}

	public Integer getIdError() {
		return idError;
	}

	public void setIdError(Integer idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
}
