package com.addcel.token.tokenizer.bean.srpago;

public class SrPagoResponse {

	private Boolean success;
	private SrPagoData data;
	private String errors;
	private SrPagoBaseResponse response;
	
	public SrPagoResponse() {}
	
	public SrPagoResponse(Boolean success, SrPagoData data, String errors, SrPagoBaseResponse response) {
		super();
		this.success = success;
		this.data = data;
		this.errors = errors;
		this.response = response;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public SrPagoData getData() {
		return data;
	}

	public void setData(SrPagoData data) {
		this.data = data;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public SrPagoBaseResponse getResponse() {
		return response;
	}

	public void setResponse(SrPagoBaseResponse response) {
		this.response = response;
	}
	
}
