package com.addcel.token.tokenizer.bean;

import com.addcel.token.tokenizer.bean.srpago.SrPagoResponse;

public class SrPagoCheckout extends BaseResponse{
	
	private String checkoutId;
	private SrPagoResponse checkout;
	
	public SrPagoCheckout() {}

	public SrPagoCheckout(String checkoutId, SrPagoResponse checkout) {
		super();
		this.checkoutId = checkoutId;
		this.checkout = checkout;
	}

	public String getCheckoutId() {
		return checkoutId;
	}

	public void setCheckoutId(String checkoutId) {
		this.checkoutId = checkoutId;
	}

	public SrPagoResponse getCheckout() {
		return checkout;
	}

	public void setCheckout(SrPagoResponse checkout) {
		this.checkout = checkout;
	}
	
}
