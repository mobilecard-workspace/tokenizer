package com.addcel.token.tokenizer.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Token extends BaseResponse{

    private String token;

    private String accountId;

    private boolean secure;

    private long idUsuario;

    private String json;

    private String path3D;

    private SecurePaths paths;

    private boolean whiteList;

    public boolean isWhiteList() {
        return whiteList;
    }

    public void setWhiteList(boolean whiteList) {
        this.whiteList = whiteList;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isSecure() {
        return secure;
    }

    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getPath3D() {
        return path3D;
    }

    public void setPath3D(String path3D) {
        this.path3D = path3D;
    }

    public SecurePaths getPaths() {
        return paths;
    }

    public void setPaths(SecurePaths paths) {
        this.paths = paths;
    }
}
