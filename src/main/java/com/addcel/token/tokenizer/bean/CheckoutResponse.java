package com.addcel.token.tokenizer.bean;

import java.util.List;

public class CheckoutResponse extends BaseResponse{
    /**
     * checkoutId : f215d27b-38e3-428e-9c2c-79f49bb54309
     * externalId : 100
     * total : 1
     * items : ["Pago CFE"]
     * createdAt : 2019-08-28T04:16:28.002Z
     * updatedAt : 2019-08-28T04:16:28.002Z
     */

    private String checkoutId;
    private String externalId;
    private String total;
    private String createdAt;
    private String updatedAt;
    private List<String> items;

    public String getCheckoutId() {
        return checkoutId;
    }

    public void setCheckoutId(String checkoutId) {
        this.checkoutId = checkoutId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
