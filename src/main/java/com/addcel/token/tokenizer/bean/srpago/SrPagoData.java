package com.addcel.token.tokenizer.bean.srpago;

public class SrPagoData {

	private Boolean success;
	private SrPagoResult result;
	
	public SrPagoData() {}
	
	public SrPagoData(Boolean success, SrPagoResult result) {
		super();
		this.success = success;
		this.result = result;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public SrPagoResult getResult() {
		return result;
	}

	public void setResult(SrPagoResult result) {
		this.result = result;
	}
	
}
