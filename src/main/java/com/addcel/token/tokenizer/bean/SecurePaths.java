package com.addcel.token.tokenizer.bean;

public class SecurePaths {

    private String start;

    private String form;

    private String authError;

    private String error;

    private String success;

    public String getStart() {
        return start;
    }

    public String getForm() {
        return form;
    }

    public String getAuthError() {
        return authError;
    }

    public String getError() {
        return error;
    }

    public String getSuccess() {
        return success;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public void setAuthError(String authError) {
        this.authError = authError;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
