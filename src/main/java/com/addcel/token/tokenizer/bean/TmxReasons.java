package com.addcel.token.tokenizer.bean;

public class TmxReasons {

	private String rule;

    private String score;

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
	
}
