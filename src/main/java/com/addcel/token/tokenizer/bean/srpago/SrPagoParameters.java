package com.addcel.token.tokenizer.bean.srpago;

public class SrPagoParameters {

	private String name;
	private String value;
	
	public SrPagoParameters() {}
	
	public SrPagoParameters(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
