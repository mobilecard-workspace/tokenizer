package com.addcel.token.tokenizer.bean;

import java.util.List;

public class Checkout extends BaseResponse{
    /**
     * apiKey : RGD9XNN-P3V4S3N-K9Z4ACE-SNKW64K
     * externalId : 100
     * items : ["Pago CFE"]
     * total : 1
     */

    private String apiKey;
    private String externalId;
    private double total;
    private List<String> items;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
